package com.example.wlady.videoclub.Classes;

/**
 * Created by Wlady on 15-12-2017.
 */

public class Videoclub {
    private String name;
    private String direction;
    private String cellphone;
    private int image;

    public Videoclub(String name, String direction, String cellphone, int image) {
        this.name = name;
        this.direction = direction;
        this.cellphone = cellphone;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getDirection() {
        return direction;
    }

    public String getCellphone() {
        return cellphone;
    }

    public int getImage() {
        return image;
    }

    @Override
    public String toString() {
        return
                "Nombre: " + name;
    }
}
