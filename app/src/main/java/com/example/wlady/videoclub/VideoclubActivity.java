package com.example.wlady.videoclub;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wlady.videoclub.Classes.BDD;

public class VideoclubActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoclub);

        Intent i= getIntent();
        int id = Integer.parseInt(i.getStringExtra("id"));

        BDD helper = new BDD(this);
        helper.VideoclubList();

        ImageView imagen = (ImageView) findViewById(R.id.Image);
        imagen.setImageResource((helper.VideoclubList().get(id).getImage()));
        imagen.setContentDescription(helper.VideoclubList().get(id).getName());

        TextView name = (TextView) findViewById(R.id.Name);
        name.setText("Nombre: "+helper.VideoclubList().get(id).getName());

        TextView direction = (TextView) findViewById(R.id.Direction);
        direction.setText("Direccion: "+helper.VideoclubList().get(id).getDirection());

        TextView cellphone = (TextView) findViewById(R.id.Cellphone);
        cellphone.setText("Numero telefónico o celular: "+helper.VideoclubList().get(id).getCellphone());
    }

    public void ReturnClick(View v){
        Intent intent=new Intent (this,MenuActivity.class);
        startActivity(intent);
    }
}
