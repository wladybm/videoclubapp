package com.example.wlady.videoclub;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.wlady.videoclub.Classes.BDD;
import com.example.wlady.videoclub.Classes.User;

public class SigninActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
    }

    public void SigninClick(View v){
        String name=((EditText) findViewById(R.id.name)).getText().toString();
        String user=((EditText)findViewById(R.id.user)).getText().toString();
        String pass=((EditText) findViewById(R.id.pass)).getText().toString();

        if(name.compareTo("")!=0 && user.compareTo("")!=0 && pass.compareTo("")!=0){
            BDD helper = new BDD(this);
            User u=new User(name,user,pass);
            helper.insertUser(u);
            Toast.makeText(this, "Registro exitoso, volviendo a la pantalla principal", Toast.LENGTH_SHORT).show();
            finish();
            Intent intent=new Intent(this, LoginActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(this, "Por favor llene los campos", Toast.LENGTH_SHORT).show();
        }
    }
}
