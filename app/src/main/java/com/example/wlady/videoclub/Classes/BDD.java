package com.example.wlady.videoclub.Classes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.wlady.videoclub.R;

import java.util.ArrayList;
import java.util.List;

public class BDD extends SQLiteOpenHelper {
    private static final String DB_NAME = "bd_videoclub";
    private static final int DB_VERSION = 1;


    public BDD(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sqlUsuarios= "CREATE TABLE USERS("+
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "NAME TEXT, USER TEXT, PASS TEXT);";
        sqLiteDatabase.execSQL(sqlUsuarios);
        String sqlVideoclubs= "CREATE TABLE VIDEOCLUBS("+
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "NAME TEXT, DIRECTION TEXT, CELLPHONE TEXT , IMAGE INTEGER);";
        sqLiteDatabase.execSQL(sqlVideoclubs);
        sqLiteDatabase.execSQL("INSERT INTO USERS(NAME, USER, PASS) VALUES('wlady','wlady@live.cl','secret');");
        this.insertVideoclub(sqLiteDatabase, new Videoclub("Videoclub Planet","Av. España 1341","6546546", R.drawable.planet));
        this.insertVideoclub(sqLiteDatabase, new Videoclub("Videocenter","Chiloe 715","65565633", R.drawable.videocenter));
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXIST USERS");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTE VIDEOCLUBS");
        onCreate(sqLiteDatabase);
    }

    //Ingresar usuarios
    public void insertUser( User user) {
        ContentValues valores = new ContentValues();
        valores.put("NAME", user.getName());
        valores.put("USER", user.getUser());
        valores.put("PASS", user.getPassword());
        getWritableDatabase().insert("USERS", null, valores);
    }

    //Ingresar videoclubs
    public void insertVideoclub(SQLiteDatabase db, Videoclub videoclub) {
        ContentValues valores = new ContentValues();
        valores.put("NAME", videoclub.getName());
        valores.put("DIRECTION", videoclub.getDirection());
        valores.put("CELLPHONE", videoclub.getCellphone());
        valores.put("IMAGE", videoclub.getImage());
       db.insert("VIDEOCLUBS", null, valores);
    }

    //Verificar login
    public boolean Login (String user, String pass){
        Cursor cursor= getReadableDatabase().rawQuery("SELECT USER, PASS FROM USERS WHERE USER='"+user+"' and PASS='"+pass+"'", null);
        if(cursor.moveToFirst()==true){
            return true;
        }else{
            return false;
        }
    }

    //Mostrar videoclubs
    public List<Videoclub> VideoclubList() {
        List<Videoclub> videoclubs = new ArrayList<>();
        String sqlTxt = "SELECT NAME, DIRECTION, CELLPHONE, IMAGE FROM VIDEOCLUBS";
        try {
            Cursor cursor = getReadableDatabase().rawQuery(sqlTxt, null);
            cursor.moveToFirst();
            Videoclub v;
            do {
                v = new Videoclub(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3));
                videoclubs.add(v);
            } while (cursor.moveToNext());
        } catch (CursorIndexOutOfBoundsException e) {
            videoclubs = null;
        }
        return videoclubs;
    }

}