package com.example.wlady.videoclub.Classes;

/**
 * Created by Wlady on 15-12-2017.
 */

public class User {
    private String name;
    private String user;
    private String password;

    public User(String name, String user, String password) {
        this.name = name;
        this.user = user;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'';
    }
}
