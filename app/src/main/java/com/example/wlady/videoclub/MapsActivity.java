package com.example.wlady.videoclub;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng videocenter = new LatLng(-53.158557,-70.906962);
        mMap.addMarker(new MarkerOptions().position(videocenter).title("Videocenter"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(videocenter));

        LatLng planet = new LatLng(-53.139016,-70.891239);
        mMap.addMarker(new MarkerOptions().position(planet).title("Videoclub Planet"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(planet));

        LatLng plaza = new LatLng(-53.162811,-70.908417);
        mMap.addMarker(new MarkerOptions().position(plaza).title("Plaza Muñoz Gamero"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(plaza));

    }
}
