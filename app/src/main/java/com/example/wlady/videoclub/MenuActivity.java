package com.example.wlady.videoclub;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wlady.videoclub.Classes.BDD;
import com.example.wlady.videoclub.Classes.Videoclub;

import java.util.List;

public class MenuActivity extends AppCompatActivity {

    private BDD helper = new BDD(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        final Intent intent = new Intent(this, VideoclubActivity.class);
        List<Videoclub> videoclubs = helper.VideoclubList();
        if(videoclubs == null){
            TextView mensaje= (TextView) findViewById(R.id.mensaje);
            mensaje.setText("No hay videoclubs disponibles :(");
        }else{
            ListView list= (ListView) findViewById(R.id.videoclub_list);
            ArrayAdapter<Videoclub> adapter = new ArrayAdapter<Videoclub>(
            this,android.R.layout.simple_list_item_1,videoclubs);
            list.setAdapter(adapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                    String pos=String.valueOf(i);
                    intent.putExtra("id",pos);
                    startActivity(intent);
                }
            });
        }
    }

    public void MapsClick(View v){
        Intent intent=new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    public void ReturnClick(View v){
        Intent intent=new Intent(this, LoginActivity.class);
        Toast.makeText(this, "Cerrando sesión", Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }

}
