package com.example.wlady.videoclub;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.wlady.videoclub.Classes.BDD;
import com.example.wlady.videoclub.Classes.Videoclub;

public class LoginActivity extends AppCompatActivity {

    private BDD helper = new BDD(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void LoginClick (View v){
        String user=((EditText) findViewById(R.id.user)).getText().toString();
        String pass=((EditText) findViewById(R.id.pass)).getText().toString();

        Intent intent=new Intent(this, MenuActivity.class);

        if(user.compareTo("")!=0 && pass.compareTo("")!=0){
            boolean login = helper.Login(user, pass);
            if(login == true){
                Toast.makeText(this, "Bienvenido "+user, Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }else{
                Toast.makeText(this, "Por favor ingrese bien los datos", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "Por favor llene los campos", Toast.LENGTH_SHORT).show();
        }
    }

    public void SigninClick(View v){
        Intent intent=new Intent(this, SigninActivity.class);
        startActivity(intent);
    }
}
